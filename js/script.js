(function(){


    var divHtml = document.getElementById('idHtml');
    var divCss = document.getElementById('idCss');
    var divAffichage = document.getElementById('container');
    var stl = document.getElementById('stl');

	var editorHtml = CodeMirror.fromTextArea(divHtml, {
        lineNumbers: true, // Нумеровать каждую строчку.
        matchBrackets: true,
        mode: "htmlmixed",
        indentUnit: 2, // Длина отступа в пробелах.
        indentWithTabs: true,
        enterMode: "keep",
        tabMode: "shift",
        theme: 'blackboard'
      });
	var editorCss = CodeMirror.fromTextArea(divCss, {
        lineNumbers: true, // Нумеровать каждую строчку.
        matchBrackets: true,
        mode: "css",
        indentUnit: 2, // Длина отступа в пробелах.
        indentWithTabs: true,
        enterMode: "keep",
        tabMode: "shift",
        theme: 'blackboard'
      });
	editorHtml.getWrapperElement().addEventListener('keyup',function(){
 		divAffichage.innerHTML = editorHtml.getValue();
 		//console.log(editorHtml.getValue())
 	})
	editorCss.getWrapperElement().addEventListener('keyup',function(){
		stl.innerHTML = editorCss.getValue();
 	})


 	/*divHtml.addEventListener('keyup',function(){
 		divAffichage.innerHTML = this.value;
 		console.log(editorHtml.getValue())
 		//divAffichage.value = this.value;
 		//myCodeMirror.va = this.value;
 		//myCodeMirror.setValue(this.value)
 		//editor.setValue(this.value)
 	})*/
 	divCss.addEventListener('keyup',function(){
 		stl.innerHTML = this.value;
 	})

 	//document.createElement("style").innerHTML = '';
 	//var myCodeMirror = CodeMirror(divAffichage); document.body
 	/*var myCodeMirror = CodeMirror(divAffichage, {
	  value: "function myScript(){return 100;}\n",
	  mode:  "javascript"
	});*/
	
})();

	